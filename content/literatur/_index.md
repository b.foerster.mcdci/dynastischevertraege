---
title: "Literatur und Quellen"
cascade:
  featured_image: '/img/literatur.jpg'

---

<h1> Literatur und Quellen </h1>

<html>
  <head>
    <title>Title of the document</title>
    <style>
      .center-cropped {
        width: 800px;
        height: 200px;
        background-image: url('/lit.png');
        background-position: center center;
        background-repeat: no-repeat;
        overflow: hidden;
      }
      .center-cropped img {
        min-height: 100%;
        min-width: 100%;
        opacity: 0;
      }
    </style>
  </head>
  <body>
    <div class="center-cropped">
      <img src="lit.png" /></div>
      <a href="https://www.digitale-sammlungen.de/de/view/bsb10491322?page=,1"><div style="text-indent:185px;"><font size="-3">Quelle: DuMont: Corps Universel du Droit des Gens, Bd. V, 2, 1728, S. 476</font></div> </a>
  </body>
</html>



Nachfolgend finden Sie verschiedene Listen mit den vollständigen Angaben der Quelleneditionen und Forschungsliteratur, die wir im Projekt verwenden. Außerdem finden Sie eine Liste, mit deren Hilfe Sie die Kurzbezeichnungen der in der Vertragsdatenbank angegeben Archive aufschlüsseln können. Außerdem finden Sie hier eine interaktive PDF-Datei mit allen Glossareinträgen.

Alle Listen und Dokumente können als PDF-Dateien heruntergeladen werden. Die Listen werden von uns regelmäßig aktualisiert. Sollten Sie dennoch in der Datenbank oder im Glossar Angaben zu Archiven, Editionen oder Literaturtiteln finden, die Sie mithilfe dieser Listen nicht aufschlüsseln können, bitten wir Sie um eine Rückmeldung, damit wir dies beheben können.

Downloads:

[Vollständige Literaturangaben zum Glossar](/literatur.pdf)<br>
[Quellen und Literatur zur Vertragsdatenbank](/quellen.pdf)<br>
[Angaben zu Archiven](/archive.pdf)<br>
[Glossar als interaktives PDF](/glossar.pdf)
