---
title: "Über das Projekt"
description: "Warum ist die Erforschung dynastischer Ehen und Eheverträge der Frühen Neuzeit für die Geschichtswissenschaft so interessant und woher stammen unsere Daten? Erfahren Sie mehr über das Projekt und seine Ziele!"

---

<h1> Über das Projekt </h1>

<div style="text-align:left">

{{< figure src="/images/bild1.jpg" caption="<span class=\"figure-number\">Quelle: </span>[Germanisches Nationalmuseum Nürnberg](https://objektkatalog.gnm.de/objekt/LGA5515), Vermählung des Kurfürsten Friedrich V. von der Pfalz mit der Prinzessin Elisabeth von England, Kupferstich von 1613" >}}



{{< details "Dynastische Ehepolitik in der Frühen Neuzeit" >}}
„The Founding Fathers of International Relations Theory Loved War but Overlooked Sex“. So lautete im Februar 2021 die Überschrift eines Artikels des US-amerikanischen Politikmagazins [Foreign Policy](https://foreignpolicy.com/2021/02/14/the-founding-fathers-of-international-relations-theory-loved-war-but-overlooked-sex/ ). Die moderne Politikwissenschaft, so konstatierte der Autor, habe eine der wichtigsten historischen Dynamiken ‚internationaler‘ Politik konsequent ignoriert: dynastische Heiratspolitik.

Der Befund verwundert insofern, als dynastische Ehen, ihr Zustandekommen und ihre Auswirkungen in der Geschichtswissenschaft schon lange als wichtiges Thema erkannt worden sind. Das gilt besonders mit Blick auf die europäische Frühe Neuzeit zwischen ca. 1500 und 1800, die den Aufstieg u. a. ehepolitisch begründeter Großreiche wie dem der Habsburger erlebte.

Dynastische Heiratspolitik der Frühen Neuzeit ist für die historische Forschung insofern bedeutsam, als der dynastische Fürstenstaat ab der Zeit um 1500 zur dominierenden Form des politischen Gemeinwesens im frühneuzeitlichen Europa wurde und es bis zum Ende der Epoche um 1800 blieb. Monarchinnen und Monarchen prägten die Geschicke Europas mit ihrer an den Ansprüchen ihres dynastischen Familienverbands orientierten Politik in entscheidender Weise. Die Ehepolitik der großen Fürstenhäuser war deshalb ein zentraler Bestandteil der politischen Beziehungen zwischen den Gemeinwesen und Staaten Europas. Dadurch erlangte sie während der Frühen Neuzeit grundsätzliche Relevanz für Sicherheit und Frieden.

Die Geschichtswissenschaft hat die Frage, wie sich dynastische Ehepolitik konkret auswirkte, aber durchaus unterschiedlich beantwortet. So wird einerseits angenommen, dass fürstliche Heiratspolitik ein Stabilitäts- und Friedenshindernis und eine Hauptursache für Konflikte und Kriege sein konnte, da sie u. a. auf Rangerhöhungen und Territorialerwerb gezielt habe. Andererseits findet sich die Annahme, Europas Fürstenhäuser hätten durch ihre Ehepolitik und Eheverträge versucht, einen ‚Mehr‘ an Sicherheit und Frieden zu erreichen.

Neue Forschungsergebnisse deuten darauf hin, dass es den politischen Akteuren fürstlicher Ehepolitik in der Frühen Neuzeit allerdings vor allem darum ging, Möglichkeitsräume für dynastisches Handeln zu schaffen, anstatt groß angelegte Friedensentwürfe zu realisieren. Insgesamt konnte dynastische Ehepolitik in einem politischen und konfessionellen Umfeld, das, wie der Historiker Johannes Burkhardt feststellt, durch ein strukturelles Friedensdefizit gekennzeichnet war, als Indikator struktureller Sicherheitsprobleme erscheinen, aber auch auf Lösung jener Probleme hindeuten.
{{< /details >}}

{{< details "Eheverträge als Quellen" >}}
Die Bedeutung und Tragweite dynastischer Ehepolitik in der Frühen Neuzeit lässt sich u. a. an den langwierigen Verhandlungen über Eheverträge erkennen, die fürstlichen Eheschließungen üblicherweise vorausgingen. Solche Eheverträge bieten Aufschluss über verschiedene Sicherheitsprobleme, die im Rahmen dynastischer Heiratspolitik von Bedeutung waren, und werfen ein Schlaglicht auf dynastische Sicherheits- und Ehrvorstellungen fürstlicher Akteure in der Frühen Neuzeit.

Als Quellen sind sie für eine Vielzahl historischer Fragestellungen von Belang: So lassen dynastische Eheverträge nicht nur Aufschlüsse über das Verhältnis der beteiligten Fürstenhäuser und die Praxis dynastischer Eheschließungen zu, sondern beinhalten auch umfangreiche personen-, güter- und vermögens- und erbrechtliche Bestimmungen. Die Gesamterschließung der Eheverträge königlicher Häuser in einer [Datenbank]({{<ref "/post">}}) ermöglicht somit einen umfassenden und vergleichenden Zugriff auf eine breite Quellenbasis zu verschiedenen Fragen der europäischen Geschichte der Frühen Neuzeit.
{{< /details >}}

{{< details "Das Datenbankprojekt im SFB/TRR 138 „Dynamiken der Sicherheit“ " >}}
Unsere [Datenbank]({{<ref "/post">}}) Eheverträge der Frühen Neuzeit 1500–1800 gehört zum Teilprojekt A03 Dynastische Eheverträge und Versicherheitlichung im [Sonderforschungsbereich/Transregio 138 Dynamiken der Sicherheit](https://www.sfb138.de/). Der SFB/TRR 138 umfasst verschiedene Teilprojekte an der [Philipps-Universität Marburg](https://www.uni-marburg.de/de), der [Justus-Liebig-Universität Gießen](https://www.uni-giessen.de/de/index) sowie am [Herder-Institut für historische Ostmitteleuropaforschung](https://www.herder-institut.de/en/welcome/) in Marburg.

Ziel des SFB ist die Erforschung der geschichtlichen Entwicklung von Sicherheitsvorstellungen und ihrer sukzessiven Integration in politische Prozesse und der Bedeutung und Auswirkungen, die sie dort haben. Im Fokus stehen dabei die Darstellung und die Herstellung von Sicherheit. Beide Prozesse sind historisch unmittelbar aufeinander bezogen und werden im Rahmen des SFB in ihrem Verhältnis zueinander und in historisch unterschiedlichen Dynamiken und Prozessstrukturen erforscht.

In diesem Kontext zielt das Teilprojekt A03 mit seiner Datenbank darauf, die für frühneuzeitliche Akteure relevanten Sicherheitsaspekte dynastischer Eheverträge systematisch zu erfassen und zu erschließen. Dynastische Eheverträge stellen aber auch über den Forschungsschwerpunkt des SFB/TRR 138 hinaus zentrale Quellen zur Erforschung der europäischen Politik-, Kultur-, Rechts- und Geschlechtergeschichte der Frühen Neuzeit dar. Die Datenbank bietet daher eine breite Datenbasis, auf die Forschende aller historischen Fachrichtungen für ihre Projekte zurückgreifen können.

Im Gegensatz zu vielen anderen geschichtswissenschaftlichen Datenbanken verzichten wir auf eine Verfügbarmachung von Digitalisaten der Originalquellen oder eine vollständige digitale Edition der Vertragstexte. Stattdessen bietet unsere Datenbank Regesten der ausgewerteten Eheverträge, die mit weiteren Informationsangeboten verknüpft werden (z. B. durch Verlinkung zur [GND](https://www.dnb.de/DE/Professionell/Standardisierung/GND/gnd_node.html) und Angaben beinhalten, in welchem Archiv oder welcher Edition der vollständige Vertragstext gefunden werden kann. Sofern Digitalisate der Quellen im Web verfügbar sind, verlinken wir auch auf diese. Zusätzlich bieten wir unsere Datensätze als PDF zum Download an.
{{< /details >}}


{{< details "Datenerhebung und Inhalt der Vertragsregesten" >}}
Im Zentrum des Datenbankvorhabens steht die forschungsorientierte Aufbereitung und Bereitstellung der Inhalte dynastischer Eheverträge aus dem Zeitraum zwischen ca. 1500–1800. In die Datenbank aufgenommen und ausgewertet werden Eheverträge zwischen Fürstenhäusern, von denen mindestens eines entweder zum Zeitpunkt der Eheschließung königlichen Status besaß oder diesen im Verlauf der Frühen Neuzeit erlangte.

In einem ersten Schritt wurden dafür rund 400 Fürstenehen ermittelt, die diesen Kriterien entsprechen. Zurückgegriffen wurde dafür v. a. auf das 1935/36 von Wilhelm Karl von Isenburg begründete genealogische Nachschlagwerk Stammtafeln zur Geschichte der europäischen Staaten, das ab 1968 als Europäische Stammtafeln fortgesetzt worden ist.

In einem zweiten Schritt wurde ein Analyseraster zur Auswertung und Regestierung der Verträge entwickelt. Informationen über die beteiligten Dynastien und Gemeinwesen sowie die konkret involvierten politischen Akteure werden ebenso aufgenommen wie die Vertragsinhalte. Dokumentiert werden überdies sukzessions- und erbrechtliche sowie konfessionelle Bestimmungen, Beteiligungen ständischer oder auswärtiger Instanzen, begleitende vertragliche Regelungen und der historische Kontext.

Die Regesten der ausgewerteten Eheverträge beinhalten außerdem Verweise auf archivalische und gedruckte Überlieferungen der Vertragstexte. Wenn möglich wird dabei auf im Web verfügbare Digitalisate verlinkt. Enthalten sind auch Links zur Gemeinsamen Normdatei ([GND](https://www.dnb.de/DE/Professionell/Standardisierung/GND/gnd_node.html)), sofern GND-Datensätze zu den im Vertrag relevanten Personen und Akteuren vorhanden sind.
{{< /details >}}

{{< details "Benutzung der Datenbank" >}}
Auf  der Unterseite „Verträge“ gelangen Sie zu unserer [Datenbankanwendung]({{<ref "/post">}}). Sie ist das Herzstück unseres Projekts. Eine **ausfühliche Anleitung und Tipps zur Suche und Benutzung** finden Sie [hier]({{<ref "/suche">}}). Die wichtigsten Funktionen stellen wir Ihnen nachfolgend in Kurzform vor:

**Suchfeld**: Durchsuchen Sie die Datenbank mithilfe selbst gewählter Suchbegriffe.

**Zeitstrahl**: Verändern Sie Ihren Suchzeitraum mithilfe des praktischen Zeitstrahls.

**Suchbegriff hinzufügen**: Fügen Sie einen Suchbegriff hinzu, um Ihre Suche einzugrenzen.

**Filter hinzufügen**: Benutzen Sie die Filter, um Verträge anhand inhaltlicher Kategorien zu filtern und nach den beteiligten Dynastien zu suchen.

**Liste alle Verträge**: Die Liste aller in der Datenbank verfügbaren Verträge variiert je nach Auswahl Ihrer Such- und Filterkriterien. Sie können die Liste chronologisch und alphabetisch sortieren.

**Vertragsdetails einblenden**: Wählen Sie Verträge aus der Liste aus, um sich die Vertragsdetails anzeigen zu lassen. Die Vertragsregesten werden in einer praktischen Übersicht als Drop-down-Menü dargestellt. Klappen Sie einzelnen Elemente aus, um  Detailinformationen zum Vertrag zu erhalten.

**Hinweis zu den Datenfeldern**: Mit „–“ ausgefüllte Datenfelder in den Vertragsregesten bedeuten, dass uns auf dem aktuellen Bearbeitungsstand keine Daten vorliegen. Dies kann sich im weiteren Projektverlauf aber ändern.

**Export der Datensätze als PDF oder JSON**: Einzelne Datensätze lassen sich in den Formaten PDF und JSON exportieren. Mit einen Rechtsklick auf „Download JSON“ können Sie den Datensatz außerdem als JSON in einem neuen Browser-Tab oder -Fenster öffnen. In der JSON-Ansicht verfügt der Datensatz über eine eigene URL, die angegeben werden kann, wenn man z. B. in einer Publikation auf diesen Datensatz verweisen möchte.

Bei Fragen zur Funktionsweise der Datenbank nutzen Sie gerne auch unser Feedbackformular, um uns zu kontaktieren. Wir antworten Ihnen so schnell wie möglich!
{{< /details >}}

{{< details "Glossar" >}}
Unsere Vertragsregesten beinhalten zahlreiche Quellenbegriffe, die in den für das Projekt ausgewerteten dynastischen Eheverträgen vorkommen und auch in der Forschungsliteratur Verwendung finden. Da wir aber davon ausgehen, dass Begriffe wie Leibgedinge, Morgengabe oder Widerfall nicht zwingend als selbsterklärend angesehen werden können, haben wir ein interaktives [Glossar]({{<ref "/glossar">}}) erstellt, das die wichtigsten Begrifflichkeiten ausführlich erläutert.
{{< /details >}}

{{< details "Feedback" >}}
Wir freuen uns über das Feedback unserer Nutzerinnen und Nutzer! Wenn Sie Fragen, Anregungen oder Ergänzungen zu Projekt, Datenbank und Website haben, uns etwas zu einem bestimmten Vertrag bzw. Regest mitteilen möchten, einen Fehler in unseren Daten entdeckt haben oder Informationen über Verträge, Editionen oder Archivalien besitzen, die für unser Projekt von Bedeutung sind, die wir aber vielleicht noch nicht kennen, können Sie uns jederzeit gerne kontaktieren. Nutzen Sie dazu unser Feedback- und Kontaktformular. Alternativ finden Sie unterhalb jedes Vertragsregests in der Datenbank die Möglichkeit, uns eine E-Mail zu schicken.
{{< /details >}}

{{< details "Literatur und Quellen" >}}
In den Vertragsregesten, die Sie in unserer Date[Datenbank]({{<ref "/post">}})nbank finden, sowie im wissenschaftlichen [Glossar]({{<ref "/glossar">}}) sind die verwendeten Quelleneditionen und Literaturtitel nur mit Kurztiteln angegeben. Die Archive, in deren Beständen wir Originalverträge nachweisen konnten, sind ebenfalls nur mit Kurzangaben vermerkt (z. B. HStAM für das Hessische Staatsarchiv in Marburg). Damit Sie die Kurztitel und Kurzbezeichnungen aufschlüsseln können, bieten wir Ihnen unter dem Menüpunkt [Literatur und Quellen]({{<ref "/literatur">}}) alle vollständigen Quellen- und Literaturangaben im PDF-Format zum Download an. Die Listen werden regelmäßig für Sie aktualisiert.
{{< /details >}}

{{< details "Bildnachweise" >}}
Nachweise der auf der Website verwendeten Abbildungen:

Bühnenbild „Startseite“: Widmungsblatt: Vermählung des Kurfürsten Friedrich V. von der Pfalz mit der Prinzessin Elisabeth von England, anonymer Kupferstich von 1613. Verwendung mit freundlicher Genehmigung des [Germanischen Nationalmuseums Nürnberg](https://objektkatalog.gnm.de/objekt/LGA5515).

Bühnenbild „Projekt“: Widmungsblatt: Vermählung des Kurfürsten Friedrich V. von der Pfalz mit der Prinzessin Elisabeth von England, anonymer Kupferstich von 1613. Verwendung mit freundlicher Genehmigung des [Germanischen Nationalmuseums Nürnberg](https://objektkatalog.gnm.de/objekt/LGA5515).

Bühnenbild „Quellen & Literatur“: DuMont, Jean: Corps Universel du Droit des Gens; contenant vn Recueil des Traitez d’Alliance, de Paix, de Treve, de Neutralité, de Commerce, d’Echange, de Protection & de Garantie, de toutes les Conventions, Transactions, Pactes, Concordats, & autres Contrats, qui ont été faits en Europe, depuis le Regne de l’Empereur Charlemagne jusques à present […]. Bd. V, 2. Amsterdam 1728, S. 476. Digitalisat online zur Verfügung gestellt durch die [Bayerische Staatsbibliothek](https://www.digitale-sammlungen.de/de/view/bsb10491322?page=,1) ohne Urheberrechtsschutz.

C2A-Teaser „Über das Projekt“ auf der Startseite: farblich veränderter Ausschnitt aus: Widmungsblatt: Vermählung des Kurfürsten Friedrich V. von der Pfalz mit der Prinzessin Elisabeth von England, anonymer Kupferstich von 1613. Verwendung mit freundlicher Genehmigung des [Germanischen Nationalmuseums Nürnberg](https://objektkatalog.gnm.de/objekt/LGA5515.

C2A-Teaser „Eheverträge“ auf der Startseite: farblich verändertes Digitalisat aus: HStAM, Urk. 3, Nr. 119: Ehevertrag zwischen Landgraf Ludwig IV. von Hessen-Marburg und Herzogin Hedwig von Württemberg. Verwendung mit freundlicher Genehmigung des [Hessischen Staatsarchivs Marburg](https://landesarchiv.hessen.de/hessisches-staatsarchiv-marburg).

C2A-Teaser „Feedback“ auf der Startseite: farblich veränderter Ausschnitt aus: [Bildnis des Erasmus von Rotterdam, Kupferstich von Albrecht Dürer 1526](https://bawue.museum-digital.de/object/4300). Verwendung mit freundlicher Genehmigung des [Graphik-Kabinetts der Stadt Backnang](http://www.galerie-der-stadt-backnang.de/gk_aktuell.php).

C2A-Teaser „Glossar“ auf der Startseite: farblich veränderter Ausschnitt aus: [Bildnis des Erasmus von Rotterdam, Kupferstich von Albrecht Dürer 1526](https://bawue.museum-digital.de/object/4300). Verwendung mit freundlicher Genehmigung des [Graphik-Kabinetts der Stadt Backnang](http://www.galerie-der-stadt-backnang.de/gk_aktuell.php).

C2A-Teaser „Quellen & Literatur“ auf der Startseite: arblich veränderter Ausschnitt aus: [Bildnis des Erasmus von Rotterdam, Kupferstich von Albrecht Dürer 1526](https://bawue.museum-digital.de/object/4300). Verwendung mit freundlicher Genehmigung des [Graphik-Kabinetts der Stadt Backnang](http://www.galerie-der-stadt-backnang.de/gk_aktuell.php).

C2A-Teaser „Tipps zur Suche“ auf der Startseite: farblich veränderter Ausschnitt aus einem Holzschnitt in: Brant, Sebastian: Nauis stultifera a domino sebastiano Brant. Basel 1507. fol. a ij verso. Digitalisat online zur Verfügung gestellt durch die [Bayerische Staatsbibliothek](https://www.digitale-sammlungen.de/de/view/bsb00005365?page=,1) unter [Creative-Commons-Lizenz](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de).
{{< /details >}}


