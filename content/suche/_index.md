---
Title: "Tipps zur suche"


---
<div style="text-align:justify">

## Tipps zur Suche

Hier finden Sie eine Anleitung und Tipps zur Suche von Verträgen in unserer Datenbank und erfahren, wie Sie die Suchwerkzeuge und Filterfunktion einsetzen können, um das gewünschte Suchergebnis zu erzielen, und welche besonderen Features die Datenbankanwendung außerdem bietet.

**Suchfeld**: Die Seite beginnt mit einem Suchfeld, in das Sie einen selbst gewählten Suchbegriff eingeben können, um die Datenbank zu durchsuchen. Beachten Sie aber: Namen von Dynastien lassen sich besser finden, wenn Sie die Suchfilter unter „Filter hinzufügen“ benutzen. Möchten Sie zwei Suchbegriffe kombinieren, dann geben sie den zweiten Begriff unter „Suchbegriff hinzufügen“ ein.

**Zeitstrahl**: Der Zeitstrahl bildet den in der Datenbank erfassten Gesamtzeitraum ab, er beginnt mit dem frühesten und endet mit dem spätesten erfassten Vertragsdatum. Die beiden Regler auf dem Zeitstrahl lassen sich verschieben, um den Zeitraum, für den Sie Verträge finden möchten, einzugrenzen oder zu erweitern.

**Suchbegriff hinzufügen**: Mithilfe dieser Funktion können Sie Suchbegriffe hinzufügen und kombinieren. Zusätzlich ausgewählte Suchbegriffe werden unterhalb der Such- und Filterwerkzeuge in kleinen ‚Kästchen‘ angezeigt. Um einen zusätzlichen Suchbegriff wieder zu löschen, klicken Sie einfach auf das „X“, das im entsprechenden Kästchen zu sehen ist.

**Filter hinzufügen**: Diese Funktion bietet Ihnen die Möglichkeit, die regestierten Verträge nach bestimmten Kriterien zu filtern. Sie können dabei auch unterschiedliche Filter miteinander kombinieren. Gehen Sie bei der Auswahl der Filter wie folgt vor: Klicken Sie auf „hinzufügen“, wählen Sie dann einen oder mehrere Filter aus und schließen Sie das Auswahlfenster durch das Klicken auf „X“. Ihre Filterauswahl wird automatisch übernommen. Genau wie ein zusätzlicher Suchbegriff werden die ausgewählten Filter unterhalb Such- und Filterwerkzeuge eingeblendet und können dort auch wieder gelöscht werden.

**Nach Dynastien filtern**: Wenn Sie die Datenbank auf Dynastien hin durchsuchen möchten, geht dies ebenfalls am besten mit der Filterfunktion. Gehen sie dazu auf „Filter hinzufügen“ dort können Sie auswählen, ob Sie die Datenbank auf die Dynastien der Braut, des Bräutigams oder der Akteure bzw. Prinzipale für Braut und Bräutigam durchsuchen möchten. Sie können diese Filter auch kombinieren.

**Filter und Suchbegriffe kombinieren**: Grundsätzlich lassen sich die Suche mithilfe von Suchbegriffen und die Filteroptionen miteinander kombiniert anwenden.

**Liste alle Verträge**: Auf der linken Bildschirmhälfte unterhalb der verschiedenen Such- und Filteroptionen sehen Sie die Liste der in der Datenbank aktuell verfügbaren Verträge. Bitte beachten Sie: Die Anzahl der in der Liste angezeigten Verträge variiert je nach Auswahl Ihrer Such- bzw. Filterkriterien!

**Sortieren der Liste**: Oberhalb der Liste der Verträge finden Sie eine Möglichkeit, die Liste chronologisch, nach den Jahren der Vertragsabschlüsse, sowie alphabetisch, nach Bezeichnungen der Verträge, zu sortieren. Sie können dies tun, indem Sie auf die Pfeilsymbole klicken, die jeweils rechts neben den Begriffen „Jahr“ und „Bezeichnung“ zu sehen sind.

**Suche zurücksetzen**: Wenn Sie möchten, dass wieder alle in der Datenbank vorhanden Verträge in der Liste angezeigt werden, entfernen Sie zunächst eventuell eingegeben zusätzliche Suchbegriffe und ausgewählte Filter, indem Sie die entsprechenden Elemente unterhalb der Such- und Filterwerkzeuge durch das Klicken aus „X“ löschen. Löschen Sie dann auch den Suchbegriff aus dem Suchfeld und klicken Sie nochmals auf das Lupensymbol oder laden Sie die Seite einfach neu.

**Vertragsdetails anzeigen**: Um die Detailansicht eines Vertragsregests anzuzeigen, klicken Sie den Vertrag in der Liste auf der linken Bildschirmseite an. Auf der rechten Bildschirmseite erscheint dann die Detailansicht zum gewählten Vertrag. Angezeigt wird zunächst eine praktische Übersicht der Auswertungskategorien, die wir auf unsere Quellen anwenden. Durch das Klicken auf die abwärts gerichteten Pfeilsymbole klappen Sie die einzelnen Kategorien auf, um mehr Informationen zu erhalten.

**Hinweis zu den Datenfeldern**: Bei der Benutzung der Datenbank ist darauf zu achten, dass frei gelassene bzw. mit „–“ ausgefüllte Felder bedeuten, dass uns auf dem aktuellen Bearbeitungsstand keine Daten für das entsprechende Feld vorliegen. Dies kann sich im weiteren Projektverlauf aber ändern, da wir nicht nur laufend weitere Verträge auswerten und regestieren, sondern auch bereits bestehende Datensätze ergänzen.

**Export der Datensätze als PDF oder JSON**: Unterhalb der Detailansicht der Vertragsregesten finden Sie eine Druckfunktion, mit deren Hilfe Sie das Regest entweder direkt ausdrucken oder auf Ihrem Computer als PDF abspeichern können. Bitte beachten Sie, vor dem Drucken alle für Sie relevanten Drop-down-Felder auszuklappen, damit diese in der Druckansicht angezeigt werden können! Alternativ können Sie den Datensatz auch als JSON (JavaScript Object Notation) exportieren.

**Datensatz als JSON im Browser öffnen**: Sie können den Datensatz im JSON-Format auch direkt im Browser öffnen, indem Sie mit der rechten Maustaste auf „Download JSON“ klicken und in dem Fenster, dass sich dann öffnet, die Optionen „Link in neuem Tab öffnen“ oder „Link in neuem Fenster öffnen“ auswählen. In der JSON-Ansicht verfügt der Datensatz über eine eigene URL, die angegeben werden kann, wenn man z. B. in einer Publikation auf diesen Datensatz verweisen möchte.

**Feedback zu Verträgen**: Falls Sie Fragen oder Anregungen zum jeweiligen Vertragsregest haben, finden Sie hier außerdem eine Kontaktmöglichkeit per E-Mail bzw. einen Link zum Feedbackformular.

Bei Fragen zur Funktionsweise der Datenbank nutzen Sie gerne auch unser Feedbackformular, um uns zu kontaktieren. Wir antworten Ihnen so schnell wie möglich!
