---
title: "Dynastische Eheverträge der frühen Neuzeit"

---

<!DOCTYPE html>
<html>
  <head>
    <title>Title of the document</title>
    <style>
      h1 {
        text-align: center;
      }
      .image {
        width: 1000px;
        height: 330px;
        margin-bottom: 20px;
        background-image: url('bild2.jpg');
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
      }
    </style>
  </head>
  <body>
<div class="image"><br></br><h1> Dynastische Eheverträge in der frühen Neuzeit</h1><h1> 1500 - 1800 </h1></div>
</html>




<div style="text-align:center">

## Dynastische Eheverträge als Zeugnisse europäischer Politik in der Frühen Neuzeit
</div>
Dynastische Eheverträge als Zeugnisse europäischer Politik in der Frühen Neuzeit
L’état, c’est moi! – Dieses Diktum wird seit dem späten 18. Jahrhundert Ludwig XIV. von Frankreich zugesprochen. Obwohl es als unwahrscheinlich gilt, dass der Sonnenkönig jenen Ausspruch tatsächlich tätigte, symbolisiert er die enge Verbindung des Monarchen – und damit der Dynastie – mit dem Staat in der Frühen Neuzeit. Durch diese Verbindung wurden Fürstenehen und dynastische Eheverträge zentrale Bestandteile der europäischen Politik der Epoche. Ihre wissenschaftliche Erforschung bietet daher Aufschluss über Prozesse und Spielregeln der monarchischen Mächtebeziehungen, welche Europa zwischen ca. 1500 und 1800 prägten.

{{<columns>}}
[![](/images/projekt.jpg "Projekt")*Über das Projekt <br> Warum wir uns mit frühzeitlichen Eheverträgen beschäftigen und sie für die Forschung relevant sind, lesen Sie hier*]({{< relref "/about/_index.md" >}})
<---> 
[![](/images/vertraege.jpg "Vertäge")*Eheverträge* <br> Hier gelangen Sie zu unserer Datenbank dynastischer Eheverträge der Frühen Neuzeit]({{< relref "/post/_index.md" >}})
<---> 
[![](/images/feedback.jpg "Feedback")*Feedback* <br>Falls Sie uns etwas mitteilen möchten, Anregungen oder Ideen haben, finden Sie hier unser Feedbackformular]({{< relref "/feedback/_index.md" >}})

{{</columns>}}

{{<columns>}}
[![](/images/glossar.jpg "Glossar")*Glossar <br>Unser wissenschaftliches Glossar erklärt Ihnen spezielle Begriffe in dynastischen Eheverträgen der Frühen Neuzeit*]({{< relref "/glossar/_index.md" >}}) 
<---> 
[![](/images/literatur.jpg "Literatur")*Literatur /& Quellen <br>Hier finden Sie weiterführende Informationen zu den Quellen und zur Fachliteratur, die wir im Projekt verwenden.*]({{< relref "/literatur/_index.md" >}}) 
<---> 
[![Bildinfo](/images/tipps.jpg "Suche")*Tipps zur Suche <br>Eine ausführliche Anleitung zur Benutzung unserer Vertragsdatenbank sowie Tipps zur Suche finden Sie hier*]({{< relref "/suche/_index.md" >}})

{{</columns>}}


