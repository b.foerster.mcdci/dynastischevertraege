---
title: "Ehevertrag Dänemark - Sachsen (Kursachsen) 1633"
categories: ["Ehevertrag", "Bräutigam:Dänemark", "Braut: Sachsen", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Oldenburg (Dänemark)", "Akteur Bräutigam:Oldenburg (Dänemark)", "Akteur Braut:Wettin (Albertiner)", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:nein", "Bräutigam:Dänemark", "Braut: Sachsen"]
tags: [ "1633"]
---
<!--more-->
{{<v10>}}