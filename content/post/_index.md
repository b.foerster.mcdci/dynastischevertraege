---
title: "Verträge"
---

<h1>Dynastische Eheverträge der frühen Neuzeit</h1>

<div>
<p><input id="search" type="text" placeholder="Enter your query"></p>

<ul id="results"></ul>

<script src="/js/jquery-2.1.3.min.js"></script>
<script src="/js/lunr.js"></script>
<script>
  var lunrIndex,
      $results,
      documents;

  function initLunr() {
    // retrieve the index file
    $.getJSON("../index.json")
      .done(function(index) {
          documents = index;

          lunrIndex = lunr(function(){
            this.ref('href')
            this.field('content')

            this.field("title", {
                boost: 10
            });

            this.field("tags", {
                boost: 5
            });

            documents.forEach(function(doc) {
              try {
                // console.log(doc.href)
                this.add(doc)
              } catch (e) {}
            }, this)
          })
      })
      .fail(function(jqxhr, textStatus, error) {
          var err = textStatus + ", " + error;
          console.error("Error getting Lunr index file:", err);
      });
  }

  function search(query) {
    return lunrIndex.search(query).map(function(result) {
      return documents.filter(function(page) {
        try {
          // console.log(page)
          return page.href === result.ref;
        } catch (e) {
          console.log('whoops')
        }
      })[0];
    });
  }

  function renderResults(results) {
    if (!results.length) {
      return;
    }

    results.slice(0, 30).forEach(function(result) {
      var $result = $("<li>");

      $result.append($("<a>", {
        href: result.href,
        text: result.title
      }));

      $result.append(" <small><time>" + result.date + "</time></small>");

      $results.append($result);
    });
  }

  function initUI() {
    $results = $("#results");

    $("#search").keyup(function(){
      // empty previous results
      $results.empty();

      // trigger search when at least two chars provided.
      var query = $(this).val();
      if (query.length < 2) {
        return;
      }

      var results = search(query);

      renderResults(results);
    });
  }

  initLunr();

  $(document).ready(function(){
    initUI();
  });
</script>
</div>
<br>

{{<columns>}}
{{<details "Akteur Braut" >}}
[Albret]({{<ref "/categories/akteur-brautalbret">}}) 
[Askanier (Sachsen-Lauenburg)]({{<ref "/categories/akteur-brautaskanier-sachsen-lauenburg">}})
[Aviz]({{<ref "/categories/akteur-brautaviz">}}) 
[Baden]({{<ref "/categories/akteur-brautbaden">}})
[Bentheim]({{<ref "/categories/akteur-brautbentheim">}}) 
[Bonaparte]({{<ref "/categories/akteur-brautbonaparte">}})
[Bourbon (Frankreich]({{<ref "/categories/akteur-brautbourbon-frankreich">}}) 
[Bourbon (Spanien)]({{<ref "/categories/akteur-brautbourbon-spanien">}})
[Braganza]({{<ref "/categories/akteur-brautbraganza">}}) 
[Braunschweig-Bevern]({{<ref "/categories/akteur-brautbraunschweig-bevern">}})
[Este]({{<ref "/categories/akteur-brauteste">}}) 
[Greifen]({{<ref "/categories/akteur-brautgreifen">}})
[Habsburg (Österreich]({{<ref "/categories/akteur-brauthabsburg-österreich">}}) 
[Habsburg (Spanien)]({{<ref "/categories/akteur-brauthabsburg-spanien">}})
[Habsburg (Lothringen]({{<ref "/categories/akteur-brauthabsburg-lothringen">}}) 
[Hanau (Münzenberg)]({{<ref "/categories/akteur-brauthanau-münzenberg/">}})
[Hessen (Kassel)]({{<ref "/categories/akteur-brauthessen-kassel/">}})
[Hessen (Rheinfels-Rotenburg)]({{<ref "/categories/akteur-brauthessen-rheinfels-rotenburg/">}})
[Hohenzollern]({{<ref "/categories/akteur-brauthohenzollern/">}})
[Jagellionen]({{<ref "/categories/akteur-brautjagellionen/">}})
[Lothringen]({{<ref "/categories/akteur-brautlothringen/">}})
[Leszczyński]({{<ref "/categories/akteur-brautleszczyński/">}})
[Meckelnburg]({{<ref "/categories/akteur-brautmecklenburg/">}})
[Medici]({{<ref "/categories/akteur-brautmedici/">}})
[Nassau-Dillenberg]({{<ref "/categories/akteur-brautnassau-dillenburg/">}})
[Nassau-Siegen]({{<ref "/categories/akteur-brautnassau-siegen/">}})
[Oldenburg (Dänemark)]({{<ref "/categories/akteur-brautoldenburg-dänemark/">}})
[Oldenburg (Gottorf)]({{<ref "/categories/akteur-brautoldenburg-gottorf/">}})
[Oranien-Nassau]({{<ref "/categories/akteur-brautoranien-nassau/">}})
[Romanow]({{<ref "/categories/akteur-brautromanow/">}})
[Sachsen-Altenburg]({{<ref "/categories/akteur-brautsachsen-altenburg/">}})
[Sachsen-Gotha-Altenburg]({{<ref "/categories/akteur-brautsachsen-gotha-altenburg/">}})
[Savoyen]({{<ref "/categories/akteur-brautsavoyen/">}})
[Schleswig-Holstein-Sonderburg-Glücksburg]({{<ref "/categories/akteur-brautschleswig-holstein-sonderburg-glücksburg/">}})
[Sobieski]({{<ref "/categories/akteur-brautsobieski/">}})
[Stuart]({{<ref "/categories/akteur-brautstuart/">}})
[Tecklenburg]({{<ref "/categories/akteur-brauttecklenburg/">}})
[Trastámara]({{<ref "/categories/akteur-brauttrastámara/">}})
[Tudor]({{<ref "/categories/akteur-brauttudor/">}})
[unbekannt]({{<ref "/categories/akteur-brautunbekannt/">}})
[Valois]({{<ref "/categories/akteur-brautvalois/">}})
[Welfen]({{<ref "/categories/akteur-brautwelfen/">}})
[Wettin (Albertiner)]({{<ref "/categories/akteur-brautwettin-albertiner/">}})
[Wittelsbach (Bayern)]({{<ref "/categories/akteur-brautwittelsbach-bayern/">}})
[Wittelsbach (Pfalz)]({{<ref "/categories/akteur-brautwittelsbach-pfalz/">}})
[Wittelsbach (Schweden)]({{<ref "/categories/akteur-brautwittelsbach-schweden/">}})
[Württemberg]({{<ref "/categories/akteur-brautwürttemberg/">}})
{{</details >}}
<---> 
{{<details "Dynastie Bräutigam" >}}
[unbekannt]({{<ref "/categories/dynastie-bräutigam/">}})
[Askanier (Anhalt)]({{<ref "/categories/dynastie-bräutigamaskanier-anhalt/">}})
[Bonaparte]({{<ref "/categories/dynastie-bräutigambonaparte/">}})
[Bourbon (Frankreich)]({{<ref "/categories/dynastie-bräutigambourbon-frankreich/">}})
[Bourbon (Spanien)]({{<ref "/categories/dynastie-bräutigambourbon-spanien/">}})
[Bourbon-Condé]({{<ref "/categories/dynastie-bräutigambourbon-condé/">}})
[Braganza]({{<ref "/categories/dynastie-bräutigambraganza/">}})
[Este]({{<ref "/categories/dynastie-bräutigameste/">}})
[Greifen]({{<ref "/categories/dynastie-bräutigamgreifen/">}})
[Habsburg (Österreich)]({{<ref "/categories/dynastie-bräutigamhabsburg-österreich/">}})
[Habsburg (Spanien)]({{<ref "/categories/dynastie-bräutigamhabsburg-spanien/">}})
[Henneberg-Schleusingen]({{<ref "/categories/dynastie-bräutigamhenneberg-schleusingen/">}})
[Hessen (Kassel)]({{<ref "/categories/dynastie-bräutigamhessen-kassel/">}})
[Hessen (Marburg)]({{<ref "/categories/dynastie-bräutigamhessen-marburg/">}})
[Hessen (Philippsthal)]({{<ref "/categories/dynastie-bräutigamhessen-philippsthal/">}})
[Hohenzollern]({{<ref "/categories/dynastie-bräutigamhohenzollern/">}})
[Jagiellonen]({{<ref "/categories/dynastie-bräutigamjagiellonen/">}})
[La Trémoille]({{<ref "/categories/dynastie-bräutigamla-trémoille/">}})
[Lothringen]({{<ref "/categories/dynastie-bräutigamlothringen/">}})
[Mark]({{<ref "/categories/dynastie-bräutigammark/">}})
[Mecklenburg]({{<ref "/categories/dynastie-bräutigammecklenburg/">}})
[Münsterberg-Oels]({{<ref "/categories/dynastie-bräutigammünsterberg-oels/">}})
[Oldenburg (Dänemark)]({{<ref "/categories/dynastie-bräutigamoldenburg-dänemark/">}})
[Oldenburg (Gottorf)]({{<ref "/categories/dynastie-bräutigamoldenburg-gottorf/">}})
[Oranien-Nassau]({{<ref "/categories/dynastie-bräutigamoranien-nassau/">}})
[Orléans]({{<ref "/categories/dynastie-bräutigamorléans/">}})
[Pfalz (Simmern)]({{<ref "/categories/dynastie-bräutigampfalz-simmern/">}})
[Romanow]({{<ref "/categories/dynastie-bräutigamromanow/">}})
[Savoyen]({{<ref "/categories/dynastie-bräutigamsavoyen/">}})
[Sforza]({{<ref "/categories/dynastie-bräutigamsforza/">}})
[Sobieski]({{<ref "/categories/dynastie-bräutigamsobieski/">}})
[Solms (Braunfels)]({{<ref "/categories/dynastie-bräutigamsolms-braunfels/">}})
[Stuart]({{<ref "/categories/dynastie-bräutigamstuart/">}})
[Trastámara]({{<ref "/categories/dynastie-bräutigamtrastámara/">}})
[Tudor]({{<ref "/categories/dynastie-bräutigamtudor/">}})
[unbekannt]({{<ref "/categories/dynastie-bräutigamunbekannt/">}})
[Vögte von Plauen]({{<ref "/categories/dynastie-bräutigamvögte-von-plauen/">}})
[Valois]({{<ref "/categories/dynastie-bräutigamvalois/">}})
[Waldeck]({{<ref "/categories/dynastie-bräutigamwaldeck/">}})
[Wasa]({{<ref "/categories/dynastie-bräutigamwasa/">}})
[Welfen]({{<ref "/categories/dynastie-bräutigamwelfen/">}})
[Wettin (Albertiner)]({{<ref "/categories/dynastie-bräutigamwettin-albertiner/">}})
[Wettin (Ernestiner)]({{<ref "/categories/dynastie-bräutigamwettin-ernestiner/">}})
[Wittelsbach (Bayern)]({{<ref "/categories/dynastie-bräutigamwittelsbach-bayern/">}})
[Wittelsbach (Pfalz)]({{<ref "/categories/dynastie-bräutigamwittelsbach-pfalz/">}})
[Wittelsbach (Schweden)]({{<ref "/categories/dynastie-bräutigamwittelsbach-schweden/">}})
[Württemberg]({{<ref "/categories/dynastie-bräutigamwürttemberg/">}})
[Zähringer]({{<ref "/categories/dynastie-bräutigamzähringer/">}})
{{</details >}}
<---> 
{{<details "Akteur Bräutigam" >}}
[unbekannt]({{<ref "/categories/akteur-bräutigam">}})
[Askanier (Anhalt)]({{<ref "/categories/akteur-bräutigamaskanier-anhalt/">}})
[Bonaparte]({{<ref "/categories/akteur-bräutigambonaparte/">}})
[Bourbon (Frankreich)]({{<ref "/categories/akteur-bräutigambourbon-frankreich/">}})
[Bourbon (Spanien)]({{<ref "/categories/akteur-bräutigambourbon-spanien/">}})
[Braganza]({{<ref "/categories/akteur-bräutigambraganza/">}})
[Este]({{<ref "/categories/akteur-bräutigameste/">}})
[Greifen]({{<ref "/categories/akteur-bräutigamgreifen/">}})
[Habsburg (Österreich)]({{<ref "/categories/akteur-bräutigamhabsburg-österreich/">}})
[Habsburg (Spanien)]({{<ref "/categories/akteur-bräutigamhabsburg-spanien/">}})
[Henneberg-Schleusingen]({{<ref "/categories/akteur-bräutigamhenneberg-schleusingen/">}})
[Hessen]({{<ref "/categories/akteur-bräutigamhessen/">}})
[Hessen (Kassel)]({{<ref "/categories/akteur-bräutigamhessen-kassel/">}})
[Hohenzollern]({{<ref "/categories/akteur-bräutigamhohenzollern/">}})
[Jagellionen]({{<ref "/categories/akteur-bräutigamjagellionen/">}})
[La Trémoille]({{<ref "/categories/akteur-bräutigamla-trémoille/">}})
[Lothringen]({{<ref "/categories/akteur-bräutigamlothringen/">}})
[Mark]({{<ref "/categories/akteur-bräutigammark/">}})
[Mecklenburg]({{<ref "/categories/akteur-bräutigammecklenburg/">}})
[Münsterberg-Oels]({{<ref "/categories/akteur-bräutigammünsterberg-oels/">}})
[Noailles]({{<ref "/categories/akteur-bräutigamnoailles/">}})
[Oldenburg (Dänemark)]({{<ref "/categories/akteur-bräutigamoldenburg-dänemark/">}})
[Oldenburg (Gottorf)]({{<ref "/categories/akteur-bräutigamoldenburg-gottorf/">}})
[Oranien-Nassau]({{<ref "/categories/akteur-bräutigamoranien-nassau/">}})
[Pfalz (Simmern)]({{<ref "/categories/akteur-bräutigampfalz-simmern/">}})
[Romanow]({{<ref "/categories/akteur-bräutigamromanow/">}})
[Savoyen]({{<ref "/categories/akteur-bräutigamsavoyen/">}})
[Schwarzburg]({{<ref "/categories/akteur-bräutigamschwarzburg/">}})
[Sforza]({{<ref "/categories/akteur-bräutigamsforza/">}})
[Sobieski]({{<ref "/categories/akteur-bräutigamsobieski/">}})
[Trastámara]({{<ref "/categories/akteur-bräutigamtrastámara/">}})
[Tudor]({{<ref "/categories/akteur-bräutigamtudor/">}})
[unbekannt]({{<ref "/categories/akteur-bräutigamunbekannt/">}})
[Valois]({{<ref "/categories/akteur-bräutigamvalois/">}})
[Vögte von Plauen]({{<ref "/categories/akteur-bräutigamvögte-von-plauen">}})
[Wasa]({{<ref "/categories/akteur-bräutigamwasa/">}})
[Welfen]({{<ref "/categories/akteur-bräutigamwelfen/">}})
[Wettin (Albertiner)]({{<ref "/categories/akteur-bräutigamwettin-albertiner/">}})
[Wettin (Ernestiner)]({{<ref "/categories/akteur-bräutigamwettin-ernestiner/">}})
[Wittelsbach (Bayern)]({{<ref "/categories/akteur-bräutigamwittelsbach-bayern/">}})
[Wittelsbach (Pfalz)]({{<ref "/categories/akteur-bräutigamwittelsbach-pfalz/">}})
[Wittelsbach (Schweden)]({{<ref "/categories/akteur-bräutigamwittelsbach-schweden/">}})
[Württemberg]({{<ref "/categories/akteur-bräutigamwürttemberg/">}})
[Zähringen]({{<ref "/categories/akteur-bräutigamzähringen/">}})
{{</details >}}
{{</columns>}}

{{<columns>}}
{{<details "Ratifikation, Bestätigungen, Genehmigungen erwähnt?" >}}
[Ja]({{<ref "/categories/ratifikationja">}}) 
[Nein]({{<ref "/categories/ratifikationnein">}}) {{</details >}}
<---> 
{{<details "Ständische Instanzen beteiligt?" >}}
[Ja]({{<ref "/categories/ständischja">}}) 
[Nein]({{<ref "/categories/ständischnein">}}) 
{{</details >}}
<---> 
{{<details "Textbezug zu vergangenen Ereignissen?" >}}
[Ja]({{<ref "/categories/textbezugja">}}) 
[Nein]({{<ref "/categories/textbezugnein">}}) 
{{</details >}}
{{</columns>}}

{{<columns>}}
{{<details "Verschiedenkonfessionelle Ehe?" >}}
[Ja]({{<ref "/categories/verschiedenkonfessionelle-eheja/">}}) 
[Nein]({{<ref "/categories/verschiedenkonfessionelle-ehenein/">}}) 
[unbekannt]({{<ref "/categories/verschiedenkonfessionelle-eheunbekannt/">}}) 
{{</details >}}
<---> 
{{<details "Eheschließung vollzogen?" >}}
[Ja]({{<ref "/categories/eheschließung-vollzogenja/">}}) 
[Nein]({{<ref "/categories/eheschließung-vollzogennein/">}}) 
{{</details >}}
<---> 
{{<details "Weitere Verträge" >}}
 [Nein]({{<ref "/categories/sonstigesnein">}}) 
 [Ja]({{<ref "/categories/sonstigesja">}}) 
{{</details >}}
{{</columns>}}

|  <div style="width:150px; height:100px; text-align:justify"></div>  | | <div style="width:150px; height:100px; text-align:justify"></div> |  |<div style="width:150px; height:100px; text-align:justify"></div>|
| :-------- | :------- |:------- |:------- |:------- |
|{{<details "Akteur Braut" >}}
[Albret]({{<ref "/categories/akteur-brautalbret">}}) 
[Askanier (Sachsen-Lauenburg)]({{<ref "/categories/akteur-brautaskanier-sachsen-lauenburg">}})
[Aviz]({{<ref "/categories/akteur-brautaviz">}}) 
[Baden]({{<ref "/categories/akteur-brautbaden">}})
[Bentheim]({{<ref "/categories/akteur-brautbentheim">}}) 
[Bonaparte]({{<ref "/categories/akteur-brautbonaparte">}})
[Bourbon (Frankreich]({{<ref "/categories/akteur-brautbourbon-frankreich">}}) 
[Bourbon (Spanien)]({{<ref "/categories/akteur-brautbourbon-spanien">}})
[Braganza]({{<ref "/categories/akteur-brautbraganza">}}) 
[Braunschweig-Bevern]({{<ref "/categories/akteur-brautbraunschweig-bevern">}})
[Este]({{<ref "/categories/akteur-brauteste">}}) 
[Greifen]({{<ref "/categories/akteur-brautgreifen">}})
[Habsburg (Österreich]({{<ref "/categories/akteur-brauthabsburg-österreich">}}) 
[Habsburg (Spanien)]({{<ref "/categories/akteur-brauthabsburg-spanien">}})
[Habsburg (Lothringen]({{<ref "/categories/akteur-brauthabsburg-lothringen">}}) 
[Hanau (Münzenberg)]({{<ref "/categories/akteur-brauthanau-münzenberg/">}})
[Hessen (Kassel)]({{<ref "/categories/akteur-brauthessen-kassel/">}})
[Hessen (Rheinfels-Rotenburg)]({{<ref "/categories/akteur-brauthessen-rheinfels-rotenburg/">}})
[Hohenzollern]({{<ref "/categories/akteur-brauthohenzollern/">}})
[Jagellionen]({{<ref "/categories/akteur-brautjagellionen/">}})
[Lothringen]({{<ref "/categories/akteur-brautlothringen/">}})
[Leszczyński]({{<ref "/categories/akteur-brautleszczyński/">}})
[Meckelnburg]({{<ref "/categories/akteur-brautmecklenburg/">}})
[Medici]({{<ref "/categories/akteur-brautmedici/">}})
[Nassau-Dillenberg]({{<ref "/categories/akteur-brautnassau-dillenburg/">}})
[Nassau-Siegen]({{<ref "/categories/akteur-brautnassau-siegen/">}})
[Oldenburg (Dänemark)]({{<ref "/categories/akteur-brautoldenburg-dänemark/">}})
[Oldenburg (Gottorf)]({{<ref "/categories/akteur-brautoldenburg-gottorf/">}})
[Oranien-Nassau]({{<ref "/categories/akteur-brautoranien-nassau/">}})
[Romanow]({{<ref "/categories/akteur-brautromanow/">}})
[Sachsen-Altenburg]({{<ref "/categories/akteur-brautsachsen-altenburg/">}})
[Sachsen-Gotha-Altenburg]({{<ref "/categories/akteur-brautsachsen-gotha-altenburg/">}})
[Savoyen]({{<ref "/categories/akteur-brautsavoyen/">}})
[Schleswig-Holstein-Sonderburg-Glücksburg]({{<ref "/categories/akteur-brautschleswig-holstein-sonderburg-glücksburg/">}})
[Sobieski]({{<ref "/categories/akteur-brautsobieski/">}})
[Stuart]({{<ref "/categories/akteur-brautstuart/">}})
[Tecklenburg]({{<ref "/categories/akteur-brauttecklenburg/">}})
[Trastámara]({{<ref "/categories/akteur-brauttrastámara/">}})
[Tudor]({{<ref "/categories/akteur-brauttudor/">}})
[unbekannt]({{<ref "/categories/akteur-brautunbekannt/">}})
[Valois]({{<ref "/categories/akteur-brautvalois/">}})
[Welfen]({{<ref "/categories/akteur-brautwelfen/">}})
[Wettin (Albertiner)]({{<ref "/categories/akteur-brautwettin-albertiner/">}})
[Wittelsbach (Bayern)]({{<ref "/categories/akteur-brautwittelsbach-bayern/">}})
[Wittelsbach (Pfalz)]({{<ref "/categories/akteur-brautwittelsbach-pfalz/">}})
[Wittelsbach (Schweden)]({{<ref "/categories/akteur-brautwittelsbach-schweden/">}})
[Württemberg]({{<ref "/categories/akteur-brautwürttemberg/">}})
{{</details >}}	||{{<details "Akteur Bräutigam" >}}
[unbekannt]({{<ref "/categories/akteur-bräutigam">}})
[Askanier (Anhalt)]({{<ref "/categories/akteur-bräutigamaskanier-anhalt/">}})
[Bonaparte]({{<ref "/categories/akteur-bräutigambonaparte/">}})
[Bourbon (Frankreich)]({{<ref "/categories/akteur-bräutigambourbon-frankreich/">}})
[Bourbon (Spanien)]({{<ref "/categories/akteur-bräutigambourbon-spanien/">}})
[Braganza]({{<ref "/categories/akteur-bräutigambraganza/">}})
[Este]({{<ref "/categories/akteur-bräutigameste/">}})
[Greifen]({{<ref "/categories/akteur-bräutigamgreifen/">}})
[Habsburg (Österreich)]({{<ref "/categories/akteur-bräutigamhabsburg-österreich/">}})
[Habsburg (Spanien)]({{<ref "/categories/akteur-bräutigamhabsburg-spanien/">}})
[Henneberg-Schleusingen]({{<ref "/categories/akteur-bräutigamhenneberg-schleusingen/">}})
[Hessen]({{<ref "/categories/akteur-bräutigamhessen/">}})
[Hessen (Kassel)]({{<ref "/categories/akteur-bräutigamhessen-kassel/">}})
[Hohenzollern]({{<ref "/categories/akteur-bräutigamhohenzollern/">}})
[Jagellionen]({{<ref "/categories/akteur-bräutigamjagellionen/">}})
[La Trémoille]({{<ref "/categories/akteur-bräutigamla-trémoille/">}})
[Lothringen]({{<ref "/categories/akteur-bräutigamlothringen/">}})
[Mark]({{<ref "/categories/akteur-bräutigammark/">}})
[Mecklenburg]({{<ref "/categories/akteur-bräutigammecklenburg/">}})
[Münsterberg-Oels]({{<ref "/categories/akteur-bräutigammünsterberg-oels/">}})
[Noailles]({{<ref "/categories/akteur-bräutigamnoailles/">}})
[Oldenburg (Dänemark)]({{<ref "/categories/akteur-bräutigamoldenburg-dänemark/">}})
[Oldenburg (Gottorf)]({{<ref "/categories/akteur-bräutigamoldenburg-gottorf/">}})
[Oranien-Nassau]({{<ref "/categories/akteur-bräutigamoranien-nassau/">}})
[Pfalz (Simmern)]({{<ref "/categories/akteur-bräutigampfalz-simmern/">}})
[Romanow]({{<ref "/categories/akteur-bräutigamromanow/">}})
[Savoyen]({{<ref "/categories/akteur-bräutigamsavoyen/">}})
[Schwarzburg]({{<ref "/categories/akteur-bräutigamschwarzburg/">}})
[Sforza]({{<ref "/categories/akteur-bräutigamsforza/">}})
[Sobieski]({{<ref "/categories/akteur-bräutigamsobieski/">}})
[Trastámara]({{<ref "/categories/akteur-bräutigamtrastámara/">}})
[Tudor]({{<ref "/categories/akteur-bräutigamtudor/">}})
[unbekannt]({{<ref "/categories/akteur-bräutigamunbekannt/">}})
[Valois]({{<ref "/categories/akteur-bräutigamvalois/">}})
[Vögte von Plauen]({{<ref "/categories/akteur-bräutigamvögte-von-plauen">}})
[Wasa]({{<ref "/categories/akteur-bräutigamwasa/">}})
[Welfen]({{<ref "/categories/akteur-bräutigamwelfen/">}})
[Wettin (Albertiner)]({{<ref "/categories/akteur-bräutigamwettin-albertiner/">}})
[Wettin (Ernestiner)]({{<ref "/categories/akteur-bräutigamwettin-ernestiner/">}})
[Wittelsbach (Bayern)]({{<ref "/categories/akteur-bräutigamwittelsbach-bayern/">}})
[Wittelsbach (Pfalz)]({{<ref "/categories/akteur-bräutigamwittelsbach-pfalz/">}})
[Wittelsbach (Schweden)]({{<ref "/categories/akteur-bräutigamwittelsbach-schweden/">}})
[Württemberg]({{<ref "/categories/akteur-bräutigamwürttemberg/">}})
[Zähringen]({{<ref "/categories/akteur-bräutigamzähringen/">}})
{{</details >}}||{{<details "Dynastie Bräutigam" >}}
[unbekannt]({{<ref "/categories/dynastie-bräutigam/">}})
[Askanier (Anhalt)]({{<ref "/categories/dynastie-bräutigamaskanier-anhalt/">}})
[Bonaparte]({{<ref "/categories/dynastie-bräutigambonaparte/">}})
[Bourbon (Frankreich)]({{<ref "/categories/dynastie-bräutigambourbon-frankreich/">}})
[Bourbon (Spanien)]({{<ref "/categories/dynastie-bräutigambourbon-spanien/">}})
[Bourbon-Condé]({{<ref "/categories/dynastie-bräutigambourbon-condé/">}})
[Braganza]({{<ref "/categories/dynastie-bräutigambraganza/">}})
[Este]({{<ref "/categories/dynastie-bräutigameste/">}})
[Greifen]({{<ref "/categories/dynastie-bräutigamgreifen/">}})
[Habsburg (Österreich)]({{<ref "/categories/dynastie-bräutigamhabsburg-österreich/">}})
[Habsburg (Spanien)]({{<ref "/categories/dynastie-bräutigamhabsburg-spanien/">}})
[Henneberg-Schleusingen]({{<ref "/categories/dynastie-bräutigamhenneberg-schleusingen/">}})
[Hessen (Kassel)]({{<ref "/categories/dynastie-bräutigamhessen-kassel/">}})
[Hessen (Marburg)]({{<ref "/categories/dynastie-bräutigamhessen-marburg/">}})
[Hessen (Philippsthal)]({{<ref "/categories/dynastie-bräutigamhessen-philippsthal/">}})
[Hohenzollern]({{<ref "/categories/dynastie-bräutigamhohenzollern/">}})
[Jagiellonen]({{<ref "/categories/dynastie-bräutigamjagiellonen/">}})
[La Trémoille]({{<ref "/categories/dynastie-bräutigamla-trémoille/">}})
[Lothringen]({{<ref "/categories/dynastie-bräutigamlothringen/">}})
[Mark]({{<ref "/categories/dynastie-bräutigammark/">}})
[Mecklenburg]({{<ref "/categories/dynastie-bräutigammecklenburg/">}})
[Münsterberg-Oels]({{<ref "/categories/dynastie-bräutigammünsterberg-oels/">}})
[Oldenburg (Dänemark)]({{<ref "/categories/dynastie-bräutigamoldenburg-dänemark/">}})
[Oldenburg (Gottorf)]({{<ref "/categories/dynastie-bräutigamoldenburg-gottorf/">}})
[Oranien-Nassau]({{<ref "/categories/dynastie-bräutigamoranien-nassau/">}})
[Orléans]({{<ref "/categories/dynastie-bräutigamorléans/">}})
[Pfalz (Simmern)]({{<ref "/categories/dynastie-bräutigampfalz-simmern/">}})
[Romanow]({{<ref "/categories/dynastie-bräutigamromanow/">}})
[Savoyen]({{<ref "/categories/dynastie-bräutigamsavoyen/">}})
[Sforza]({{<ref "/categories/dynastie-bräutigamsforza/">}})
[Sobieski]({{<ref "/categories/dynastie-bräutigamsobieski/">}})
[Solms (Braunfels)]({{<ref "/categories/dynastie-bräutigamsolms-braunfels/">}})
[Stuart]({{<ref "/categories/dynastie-bräutigamstuart/">}})
[Trastámara]({{<ref "/categories/dynastie-bräutigamtrastámara/">}})
[Tudor]({{<ref "/categories/dynastie-bräutigamtudor/">}})
[unbekannt]({{<ref "/categories/dynastie-bräutigamunbekannt/">}})
[Vögte von Plauen]({{<ref "/categories/dynastie-bräutigamvögte-von-plauen/">}})
[Valois]({{<ref "/categories/dynastie-bräutigamvalois/">}})
[Waldeck]({{<ref "/categories/dynastie-bräutigamwaldeck/">}})
[Wasa]({{<ref "/categories/dynastie-bräutigamwasa/">}})
[Welfen]({{<ref "/categories/dynastie-bräutigamwelfen/">}})
[Wettin (Albertiner)]({{<ref "/categories/dynastie-bräutigamwettin-albertiner/">}})
[Wettin (Ernestiner)]({{<ref "/categories/dynastie-bräutigamwettin-ernestiner/">}})
[Wittelsbach (Bayern)]({{<ref "/categories/dynastie-bräutigamwittelsbach-bayern/">}})
[Wittelsbach (Pfalz)]({{<ref "/categories/dynastie-bräutigamwittelsbach-pfalz/">}})
[Wittelsbach (Schweden)]({{<ref "/categories/dynastie-bräutigamwittelsbach-schweden/">}})
[Württemberg]({{<ref "/categories/dynastie-bräutigamwürttemberg/">}})
[Zähringer]({{<ref "/categories/dynastie-bräutigamzähringer/">}})
{{</details >}}|
|{{<details "Ratifikation, Bestätigungen, Genehmigungen erwähnt?" >}}
[Ja]({{<ref "/categories/ratifikationja">}}) 
[Nein]({{<ref "/categories/ratifikationnein">}}) {{</details >}}||{{<details "Weitere Verträge" >}}
[Nein]({{<ref "/categories/sonstigesnein">}}) 
[Ja]({{<ref "/categories/sonstigesja">}}) 
{{</details >}}||{{<details "Ständische Instanzen beteiligt?" >}}
[Ja]({{<ref "/categories/ständischja">}}) 
[Nein]({{<ref "/categories/ständischnein">}}) 
{{</details >}}|
|{{<details "Textbezug zu vergangenen Ereignissen?" >}}
[Ja]({{<ref "/categories/textbezugja">}}) 
[Nein]({{<ref "/categories/textbezugnein">}}) 
{{</details >}}||{{<details "Verschiedenkonfessionelle Ehe?" >}}
[Ja]({{<ref "/categories/verschiedenkonfessionelle-eheja/">}}) 
[Nein]({{<ref "/categories/verschiedenkonfessionelle-ehenein/">}}) 
[unbekannt]({{<ref "/categories/verschiedenkonfessionelle-eheunbekannt/">}}) 
{{</details >}}||{{<details "Eheschließung vollzogen?" >}}
[Ja]({{<ref "/categories/eheschließung-vollzogenja/">}}) 
[Nein]({{<ref "/categories/eheschließung-vollzogennein/">}}) 
{{</details >}}|
<br>
<h2> Alle Verträge (220)</h2>





