---
title: "Ehevertrag Sachsen - Hessen 1541"
categories: ["Ehevertrag", "Bräutigam:Sachsen", "Braut: Hessen", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Wettin (Albertiner)", "Akteur Bräutigam:Wettin (Albertiner)", "Akteur Braut:Hessen (Kassel)", "Textbezug?:ja", "Ständisch?:nein", "Ratifikation?:ja", "Sonstiges?:ja", "Bräutigam:Sachsen", "Braut: Hessen"]
tags: [ "1541"]
---
<!--more-->
{{<v92>}}