---
title: "Ehevertrag Anhalt-Harzgerode - Nassau-Dillenburg 1695 (Nebenlinie)"
categories: ["Ehevertrag", "Bräutigam:Anhalt-Harzgerode", "Braut: Nassau-Dillenburg", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:unbekannt", "Dynastie Bräutigam:Askanier (Anhalt)", "Akteur Bräutigam:Askanier (Anhalt)", "Akteur Braut:Nassau-Dillenburg", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:ja", "Sonstiges?:nein", "Bräutigam:Anhalt-Harzgerode", "Braut: Nassau-Dillenburg"]
tags: [ "1695"]
---
<!--more-->
{{<v169>}}