---
title: "Ehevertrag Braunschweig-Lüneburg-Calenberg - Brandenburg 1525"
categories: ["Ehevertrag", "Bräutigam:Braunschweig-Lüneburg-Calenberg", "Braut: Brandenburg", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Welfen", "Akteur Bräutigam:Welfen", "Akteur Braut:Hohenzollern", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:ja", "Sonstiges?:nein", "Bräutigam:Braunschweig-Lüneburg-Calenberg", "Braut: Brandenburg"]
tags: [ "1525"]
---
<!--more-->
{{<v50>}}