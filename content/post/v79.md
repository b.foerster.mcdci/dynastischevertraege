---
title: "Ehevertrag Brandenburg - Sachsen 1524"
categories: ["Ehevertrag", "Bräutigam:Brandenburg", "Braut: Sachsen", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Hohenzollern", "Akteur Bräutigam:Hohenzollern", "Akteur Braut:Wettin (Albertiner)", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:nein", "Bräutigam:Brandenburg", "Braut: Sachsen"]
tags: [ "1524"]
---
<!--more-->
{{<v79>}}