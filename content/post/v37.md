---
title: "Ehevertrag Baden-Baden - Savoyen/Frankreich 1653"
categories: ["Ehevertrag", "Bräutigam:Baden-Baden", "Braut: Savoyen/Frankreich", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Zähringer", "Akteur Bräutigam:Zähringen", "Akteur Braut:Savoyen", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:ja", "Sonstiges?:nein", "Bräutigam:Baden-Baden", "Braut: Savoyen/Frankreich"]
tags: [ "1653"]
---
<!--more-->
{{<v37>}}