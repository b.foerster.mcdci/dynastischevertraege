---
title: "Ehevertrag Jülich-Kleve-Berg - Österreich 1546"
categories: ["Ehevertrag", "Bräutigam:Jülich-Kleve-Berg", "Braut: Österreich", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Mark", "Akteur Bräutigam:Mark", "Akteur Braut:Habsburg (Österreich)", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:ja", "Bräutigam:Jülich-Kleve-Berg", "Braut: Österreich"]
tags: [ "1546"]
---
<!--more-->
{{<v107>}}