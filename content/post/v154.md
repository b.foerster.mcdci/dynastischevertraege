---
title: "Ehevertrag Mailand - Dänemark 1533"
categories: ["Ehevertrag", "Bräutigam:Mailand", "Braut: Österreich/Spanien", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Sforza", "Akteur Bräutigam:Sforza", "Akteur Braut:Habsburg (Spanien)", "Textbezug?:nein", "Ständisch?:ja", "Ratifikation?:ja", "Sonstiges?:ja", "Bräutigam:Mailand", "Braut: Österreich/Spanien"]
tags: [ "1533"]
---
<!--more-->
{{<v154>}}