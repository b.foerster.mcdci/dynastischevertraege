---
title: "Ehevertrag Brandenburg - Dänemark 1502"
categories: ["Ehevertrag", "Bräutigam:Joachim I. Kurfürst von Brandenburg", "Braut: Elisabeth von Dänemark", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Hohenzollern", "Akteur Bräutigam:Hohenzollern", "Akteur Braut:Oldenburg (Dänemark)", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:ja", "Bräutigam:Joachim I. Kurfürst von Brandenburg", "Braut: Elisabeth von Dänemark"]
tags: [ "1502"]
---
<!--more-->
{{<v132>}}