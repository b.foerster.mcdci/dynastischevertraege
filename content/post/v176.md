---
title: "Ehevertrag Frankreich - Polen 1725"
categories: ["Ehevertrag", "Bräutigam:Frankreich", "Braut: Polen", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Bourbon (Frankreich)", "Akteur Bräutigam:Bourbon (Frankreich)", "Akteur Braut:Leszczyński", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:nein", "Bräutigam:Frankreich", "Braut: Polen"]
tags: [ "1725"]
---
<!--more-->
{{<v176>}}