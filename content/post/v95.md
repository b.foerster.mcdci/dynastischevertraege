---
title: "Ehevertrag Frankreich - Hessen-Kassel 1648"
categories: ["Ehevertrag", "Bräutigam:Frankreich", "Braut: Hessen-Kassel", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:La Trémoille", "Akteur Bräutigam:La Trémoille", "Akteur Braut:Hanau (Münzenberg)", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:ja", "Sonstiges?:nein", "Bräutigam:Frankreich", "Braut: Hessen-Kassel"]
tags: [ "1648"]
---
<!--more-->
{{<v95>}}