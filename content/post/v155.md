---
title: "Ehevertrag Navarra - Frankreich 1572"
categories: ["Ehevertrag", "Bräutigam:Navarra", "Braut: Frankreich", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Ja", "Dynastie Bräutigam:Bourbon (Frankreich)", "Akteur Bräutigam:Bourbon (Frankreich)", "Akteur Braut:Valois", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:nein", "Bräutigam:Navarra", "Braut: Frankreich"]
tags: [ "1572"]
---
<!--more-->
{{<v155>}}