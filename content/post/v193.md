---
title: "Ehevertrag Schleswig-Holstein-Gottorf - Dänemark 1596"
categories: ["Ehevertrag", "Bräutigam:Schleswig-Holstein-Gottorf", "Braut: Dänemark", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Oldenburg (Gottorf)", "Akteur Bräutigam:Oldenburg (Gottorf)", "Akteur Braut:Oldenburg (Dänemark)", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:ja", "Bräutigam:Schleswig-Holstein-Gottorf", "Braut: Dänemark"]
tags: [ "1596"]
---
<!--more-->
{{<v193>}}