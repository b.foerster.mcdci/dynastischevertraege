---
title: "Ehevertrag Sachsen-Weimar - Württemberg 1583"
categories: ["Ehevertrag", "Bräutigam:Sachsen-Weimar", "Braut: Württemberg", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Wettin (Ernestiner)", "Akteur Bräutigam:Wettin (Albertiner)", "Akteur Braut:Württemberg", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:nein", "Bräutigam:Sachsen-Weimar", "Braut: Württemberg"]
tags: [ "1583"]
---
<!--more-->
{{<v77>}}