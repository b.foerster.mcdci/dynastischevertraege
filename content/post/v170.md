---
title: "Ehevertrag Savoyn-Nemours - Frankreich 1528"
categories: ["Ehevertrag", "Bräutigam:Savoyen-Piemont", "Braut: Frankreich", "Eheschließung vollzogen?:Ja", "verschiedenkonfessionelle Ehe?:Nein", "Dynastie Bräutigam:Savoyen", "Akteur Bräutigam:Savoyen", "Akteur Braut:Sachsen-Altenburg", "Textbezug?:nein", "Ständisch?:nein", "Ratifikation?:nein", "Sonstiges?:nein", "Bräutigam:Savoyen-Piemont", "Braut: Frankreich"]
tags: [ "1528"]
---
<!--more-->
{{<v170>}}